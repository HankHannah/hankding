# Interview



## Getting started
```
1. Untar Charts/service-a-1.0.0.tgz
2. cd service-a
3. helm install --namespace service-dingsp --kubeconfig /opt/app/service-a/kubeconfig.yaml --debug  service-a .
```

## Validation

```
helm list --kubeconfig /opt/app/service-a/kubeconfig.yaml --namespace service-dingsp
```

![image-20211217170341800](/Users/hankding/Library/Application Support/typora-user-images/image-20211217170341800.png)

