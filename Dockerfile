# Build the manager binary
#FROM golang:1.16 as builder
FROM  devregistry.yssredefinecloud.com/infrastructure/golang:v1.16 as builder
WORKDIR /workspace
# Copy the Go Modules manifests
COPY go.mod go.mod
COPY go.sum go.sum
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
#RUN go env -w GOPROXY=https://mirrors.aliyun.com/goproxy/,direct
RUN go env -w GOPROXY=https://goproxy.cn,direct
RUN go mod download

# Copy the go source
COPY main.go main.go
COPY pkg/ pkg/

# Build
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -o service-a main.go

FROM  devregistry.yssredefinecloud.com/infrastructure/golang:v1.16
WORKDIR /
COPY --from=builder /workspace/service-a .
COPY config.yaml .
#USER 65532:65532

ENTRYPOINT ["/service-a", "--file", "config.yaml"]

